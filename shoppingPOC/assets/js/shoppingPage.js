window.onload=function(){
    let data_load = _getShoppingItems();
    let total_items = `${data_load.length} items`;
    
    let shopping_bag_template = '';
    for(let element in data_load){
      shopping_bag_template += `<div id="inner_div"><div><img src='${data_load[element]['image']}'></div><div style='margin-left:20px'>${data_load[element]['description']}</div><div class='size1'>${data_load[element]['size']}</div><div class='qtyprice'><button>${data_load[element]['qty']}</button></div><div class='qtyprice'>${data_load[element]['price']}</div></div><hr/>`  
    };
    $("#shopping_bag").html(shopping_bag_template);
    $("#item_no").html(total_items);
}